<?php
/**
 * Class de registre
 * Sert à récupérer les Helpers dans les scopes des actions/filter WordPress
 *
 * @author Romario Sobreira <rso@wng.ch>
 * @version 1.0
 */

namespace Wng\Wplogin\Model;

wp_protect();

/**
 * Class Registry
 * Injecteur de dépendance, sera utilisé pour avoir accès aux Helpers
 * @package Wng\Wp\Model
 */
if (! class_exists('Wng\Wplogin\Model\Registry')) {
    class Registry
    {
        /**
         * @var array
         */
        private $storage = array();

        /**
         * @param string $id
         * @param mixed $class
         */
        public function add($id, $class): void
        {
            $this->storage[$id] = $class;
        }

        /**
         * @param string $id
         * @return mixed|null
         */
        public function get($id)
        {
            return $this->storage[$id] ?? null;
        }
    }
}
