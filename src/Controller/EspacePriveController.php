<?php


	namespace Wng\Wplogin\Controller;
	use Wng\Wplogin\Helper\MailHelper;
	use Wng\Wplogin\Helper\Modal;
	use Wng\Wplogin\Helper\WpHelper;

	class EspacePriveController
	{
		private $errors = "";
		private $has_error = false;

		public static function init()
		{
			WpHelper::wp_config();

			add_action('init', array(__CLASS__,'check_user_confirmation'));

			add_action('wp_ajax_esp_login', array( 'Wng\Wplogin\Helper\AjaxHelper', 'esp_login'));
			add_action('wp_ajax_nopriv_esp_login', array( 'Wng\Wplogin\Helper\AjaxHelper', 'esp_login'));

			add_action('wp_ajax_esp_signin', array( 'Wng\Wplogin\Helper\AjaxHelper', 'esp_signin'));
			add_action('wp_ajax_nopriv_esp_signin', array( 'Wng\Wplogin\Helper\AjaxHelper', 'esp_signin'));

			add_action('wp_ajax_esp_reset', array( 'Wng\Wplogin\Helper\AjaxHelper', 'esp_reset'));
			add_action('wp_ajax_nopriv_esp_reset', array( 'Wng\Wplogin\Helper\AjaxHelper', 'esp_reset'));

			add_action('wp_ajax_esp_forgot_confirm', array( 'Wng\Wplogin\Helper\AjaxHelper', 'esp_forgot_confirm'));
			add_action('wp_ajax_nopriv_esp_forgot_confirm', array( 'Wng\Wplogin\Helper\AjaxHelper', 'esp_forgot_confirm'));
		}

		/**
		 * @return bool
		 */
		public function isHasError()
		{
			return $this->has_error;
		}

		public static function add_modal_script($html, $id)
		{
			wp_enqueue_script('modal-script', get_template_directory_uri() . '/assets/scripts/alert/modal.js', array('jquery'), null, true);
			wp_localize_script('modal-script','modal_alert', [
				'html' => $html,
				'id' => $id
			] );
		}


		public static function check_user_confirmation()
		{

			// CONFIRMATION
			$token = filter_input(INPUT_GET, 'token', FILTER_SANITIZE_STRING);
			$user_id = filter_input(INPUT_GET, 'user_id', FILTER_VALIDATE_INT);

			if($token && $user_id){
				// check [token] <=> $user_id
				$check = MailHelper::check(MailHelper::CONFIRM, $token, $user_id,true);
				if ($check){

					// update champ acf 'active' de l'utilisateur
					update_field('active', 1, 'user_' . $user_id);

					// création du modal
					$modal = new Modal(
						false,
						__("Succès après votre confirmation!", 'wng-translate'),
						__("Merci, votre compte pour espace privé est bien confirmé!", 'wng-translate'),
						'confirmation_modal'
					);

					MailHelper::check(MailHelper::CONFIRM, $token, $user_id,false);
				}

				if (!$check){
					// création du modal error
					$modal = new Modal(
						true,
						__("Votre lien unique est invalide.", 'wng-translate'),
						__("Merci de regénérer votre lien unique via notre formulaire.", 'wng-translate'),
						'confirmation_modal_error'
					);
				}

				add_action('wp_enqueue_scripts', function() use ( $modal ){
					self::add_modal_script($modal::body(), $modal::$id);
				}, 20, 1);
			}
		}

		public function check_text_field($field_text, $field_name){

			// cleaning
			$text_filtered = filter_var($field_text,FILTER_SANITIZE_STRING);

			// test
			if(!$text_filtered) $this->add_error(__("Le champs {$field_name} est invalide", 'wng-translate'));

			return $text_filtered;
		}

		public function check_email_field($email){

			// cleaning
			$email_filtered = filter_var($email, FILTER_VALIDATE_EMAIL);

			// test
			if(!$email_filtered) $this->add_error(__("Votre email est invalide", 'wng-translate'));

			return $email_filtered;
		}

		public function check_user_exists($field){

			// var
			$exists = username_exists($field) || email_exists($field);

			// test
			if(!$exists) $this->add_error(__("Votre username ou votre email est invalide.", 'wng-translate'));

			return $exists;
		}

		public function check_user_not_exists($field){

			// var
			$doesnt_exists = !username_exists($field) && !email_exists($field);

			// test
			if(!$doesnt_exists) $this->add_error(__("Votre username ou votre email est déjà enregistré", 'wng-translate'));

			return $doesnt_exists;
		}

		public function check_wp_error($callback){
			if (is_wp_error($callback)) {
				$err_codes = $callback->get_error_codes();

				// Invalid username.
				if ( in_array( 'invalid_username', $err_codes ) ) {
					$this->add_error(__("Votre nom d'utilisateur est invalide", 'wng-translate'));
					return false;
				}

				// Incorrect password.
				if ( in_array( 'incorrect_password', $err_codes ) ) {
					$this->add_error( __("Votre mot de passe est incorrect", 'wng-translate') );
					return false;
				}
				$this->add_error($callback->get_error_message());
				return false;
			}
			return true;
		}

		public function add_error($error){

			// actualisation de la propriété boolean
			if(!$this->has_error) $this->has_error = true;

			// actualisation de la propriété string du texte d'erreur
			$this->errors .= '<p>'.$error.'</p>';
		}

		public function get_errors(){
			return $this->errors;
		}

		public function login_user($username, $password)
		{
			$creds = [
				'user_login' => $username,
				'user_password' => $password,
				'remember' => true,
			];

			$user = wp_signon($creds, false);

			if(!$this->check_wp_error($user)){
				return false;
			}

			$userID = $user->ID;
			wp_clear_auth_cookie();
			wp_set_current_user($userID);
			wp_set_auth_cookie($userID);
			do_action('wp_login', $username, $user);
			return true;
		}

		public function register_user($username, $email, $pwd)
		{
			// enregistrement de l'utilisateur
			$user_datas = [
				'user_login' => $username,
				'user_email' => $email,
				'user_pass' => $pwd,
				'role' => 'customer'
			];

			$user_id = wp_insert_user($user_datas);

			if(!$this->check_wp_error($user_id)){
				return false;
			}

			return $user_id;
		}

		public function get_user($user_login){

			$user = false;

			if(is_email($user_login)){
				$user = get_user_by('email', $user_login);
			}

			if(!is_email($user_login)){
				$user = get_user_by('login', $user_login);
			}

			if(!$this->check_wp_error($user_login)){
				return false;
			}

			return $user;
		}

		public function check_nonce($nonce, $action){
			if(!$nonce || !wp_verify_nonce($nonce,$action)){
				$this->add_error(__("Nonce error", 'wng-translate'));
				return false;
			}
			return true;
		}
	}