<?php
/**
 * Class Helper
 * Contiens des fontions utiles pour les templates
 *
 * @author Romario Sobreira <rso@wng.ch>
 * @version 1.0
 */

namespace Wng\Wplogin\Helper;

wp_protect();

/**
 * Class TemplateHelper
 * @package Wng\Wplogin\Helper
 */
if (! class_exists('TemplateHelper')) {
    class TemplateHelper
    {
        public static function login_form()
        {
            wwpl_get_template_part('login_form');
        }

        public static function signin_form()
        {
            wwpl_get_template_part('signin_form');
        }

        public static function forgot_pwd_form()
        {
            wwpl_get_template_part('forgot_pwd_form');
        }

        public static function forgot_pwd_confirm_form()
        {
            wwpl_get_template_part('forgot_pwd_confirm_form');
        }

        public static function form()
        {
            if (get_current_user_id())      wwpl_get_template_part('form_no_result');
            else                            wwpl_get_template_part('form');
        }
    }
}
