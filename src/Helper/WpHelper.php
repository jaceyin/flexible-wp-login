<?php

namespace Wng\Wplogin\Helper;

class WpHelper
{

    public static function wp_config()
    {
        // création du role esp-membre
        add_action('init', array(__CLASS__,'add_roles'));
        // redirection vers la homepage lorsqu'un user se déconnecte
        add_action('wp_logout', array(__CLASS__,'redirect_after_logout'));
        // remove l'admin bar si non-admin
		add_action('after_setup_theme', array(__CLASS__,'remove_admin_bar'));
        // enqueues
        add_action('wp_enqueue_scripts', array(__CLASS__, 'wng_wplogin_enqueues'));
        // définition template api download
        add_filter('page_template', array(__CLASS__, 'set_download_template'));
        // override de template Wc via plugin
        add_filter( 'woocommerce_locate_template', array(__CLASS__,'woo_adon_plugin_template'), 1, 3 );
        // Formulaire d'inscription dans template
        add_action('product_api_download', array('Wng\Wplogin\Helper\TemplateHelper', 'form') );
    }

    public static function woo_adon_plugin_template($template, $template_name, $template_path)
    {
        global $woocommerce;
        $_template = $template;
        if (!$template_path)
            $template_path = $woocommerce->template_url;

        $plugin_path  = PLUGIN_DIR_PATH  . '/templates/woocommerce/';

        // Look within passed path within the theme - this is priority
        $template = locate_template(
            array(
                $template_path . $template_name,
                $template_name
            )
        );

        if (!$template && file_exists($plugin_path . $template_name))
            $template = $plugin_path . $template_name;

        if (!$template)
            $template = $_template;

        return $template;
    }

    public static function wng_wplogin_enqueues(){
        wp_enqueue_script('wng-wplogin-js', plugin_dir_url(WNG_WP_LOGIN) . 'dist/main.js',false,false,true);
        wp_enqueue_script('wng-wplogin-modal-js', plugin_dir_url(WNG_WP_LOGIN) . 'dist/modal.js',false,false,true);
        wp_localize_script( 'wng-wplogin-js', 'ajax', array(
            'url' => admin_url( 'admin-ajax.php' ),
            'nonce' => wp_create_nonce( 'esp_login' ),
            'nonce_signin' => wp_create_nonce( 'esp_signin' ),
            'nonce_reset' => wp_create_nonce( 'esp_reset' ),
        ) );
    }

    public static function remove_admin_bar() {
        if (!current_user_can('administrator') && !is_admin()) show_admin_bar(false);
    }

    public static function redirect_after_logout(){
        wp_redirect( get_home_url() );
        exit();
    }

    public static function add_roles(){
        add_role( 'wp_login_member', __("WNG WpLogin User", 'wng-translate'), array() );
    }

    public static function set_download_template($page_template)
    {
        if ( is_page( 'product-api-download' ) ) {
            $page_template = plugin_dir_path(WNG_WP_LOGIN) . '/product-api-download.php';
        }

        return $page_template;
    }
}