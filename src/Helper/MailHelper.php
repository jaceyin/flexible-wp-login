<?php


	namespace Wng\Wplogin\Helper;


	class MailHelper
	{
		// TODO: changer Bcc après tests
		const CONFIRM = 'email-confirmation-';
		const RESET = 'email-reset-password-';
		const HEADERS = array('Content-Type: text/html; charset=UTF-8','');

		private static function signature(){
			return '';
		}

		public static function subject($type){
			return get_bloginfo( 'name' ) . ' - ' . $type;
		}

		public static function body($type){
			$email = "";

			// TODO: Créer signature
			$signature = self::signature();

			switch ($type){
				case 'esp_confirmation':
					// TODO: Créer email type de confirmation d'un utilisateur de l'espace privé
					ob_start(); ?>
					<p><?= __("Merci pour votre inscription à l'espace privé", 'wng-translate') ?></p>
					<p><a href="<?= get_home_url() ?>?%s=%s&user_id=%s"><?= __("Lien de confirmation de votre inscription", 'wng-translate') ?></a></p>
					<?php $email .= ob_get_clean();
					break;
				case 'password_reset':
					ob_start(); ?>
					<p><a href="<?= get_home_url(null,'password-reset') .'/' ?>?%s=%s&user_id=%s" target="_blank"><?= __("Lien de réinitialisation de votre mot de passe", 'wng-translate') ?></a></p>
					<?php $email .= ob_get_clean();
					break;
			}

			$email .= $signature;

			return $email;
		}

		/**
		 * Undocumented function
		 *
		 * @param string $prefix
		 * @param string $to
		 * @param string $subject
		 * @param string $message
		 * @param array $headers
		 * @param integer $user_id
		 * @return void
		 */
		public static function send_link($prefix , $to, $subject, $message, $headers, $user_id)
		{

			// token unique
			$token = sha1(uniqid('', true));
			// existe-il deja notre table option?
			$oldData = get_option($prefix .'data') ?: array();
			// $data[token] = user_id
			$data = array();
			$data[$token] = $user_id;
			// merge de notre data dans la table option old+nouvelle data
			update_option($prefix .'data', array_merge($oldData, $data));

			// ?token=$token ou ?reset=$token
			// page confirmation = ?token
			// page reset password = ?reset
			$action = self::CONFIRM === $prefix ? 'token' : 'reset';

			//send email
			wp_mail($to, $subject, sprintf($message, $action, $token, $user_id), $headers);
		}

		/**
		 * Undocumented function
		 *
		 * @param string $prefix
		 * @param string $token
		 * @param integer $user_id
		 * @param boolean $safe
		 * @return bool
		 */
		public static function check($prefix, $token, $user_id, $safe = false)
		{
			// get la table option
			$data = get_option($prefix .'data');
			// data supposé de l'user
			$user_id_in_table = $data[$token];

			// check si le data se trouve dans la table
			if (isset($user_id_in_table) && $user_id_in_table === (int)$user_id) {
				if(!$safe){
					unset($data[$token]); // unset
					update_option($prefix .'data', $data); // update de la table option après l'unset
				}
				return true;
			}

			return false;
		}
	}