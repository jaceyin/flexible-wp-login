<?php


	namespace Wng\Wplogin\Helper;
	use Wng\Wplogin\Controller\EspacePriveController;
	use Wng\Wplogin\Helper\MailHelper;
	use Wng\Wplogin\Helper\Modal;


	class AjaxHelper
	{

		public static function esp_forgot_confirm(){
			$nonce = filter_input(INPUT_POST, 'nonce', FILTER_SANITIZE_STRING);
			$pwd = filter_input(INPUT_POST, 'pwd', FILTER_SANITIZE_STRING);
			$pwdtwo = filter_input(INPUT_POST, 'pwdtwo', FILTER_SANITIZE_STRING);
			$token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING);
			$user_id = filter_input(INPUT_POST, 'user_id', FILTER_VALIDATE_INT);

			$controller = new EspacePriveController;

			// check si les valeur get sont définies correctement
			if(!$token || !$user_id){
				$controller->add_error(__("Erreur avec les paramètres de l'URL", 'wng-translate'));
			}

			// check des fields du formulaire
			if(!$controller->isHasError()){
				$controller->check_text_field($pwd,__("mot de passe", 'wng-translate'));
				$controller->check_text_field($pwdtwo,__("mot de passe de confirmation", 'wng-translate'));
				$controller->check_nonce($nonce, 'esp_forgot_confirm');
			}

			if(!$controller->isHasError()){
				// check si mdps identiques
				if($pwdtwo !== $pwd){
					$controller->add_error(__("Les mots de passe ne sont pas identiques", 'wng-translate'));
				}
			}

			// fields OK
			// gets OK
			// -> checker si les valeurs get sont définies correctement dans la table option correspondante
			if( !$controller->isHasError() ){
				if(!MailHelper::check(MailHelper::RESET, $token,$user_id, true)){
					$controller->add_error(__("Votre lien est invalide", 'wng-translate'));
				}
			}

			if( !$controller->isHasError() ){
				wp_set_password($pwd,$user_id);
			}

			if ( $controller->isHasError() ){
				$modal = new Modal(
					true,
					__("Erreur lors de la réinitialisation de votre mot de passe", 'wng-translate'),
					$controller->get_errors(),
					'signin_modal'
				);
			}

			if ( !$controller->isHasError() ){
				$modal = new Modal(
					false,
					__("Merci!", 'wng-translate'),
					__("Votre mot de passe a bien été modifié", 'wng-translate'),
					'signin_modal'
				);
				MailHelper::check(MailHelper::RESET, $token, $user_id, false);
			}

			wp_send_json([
				'modal' => $modal::body(),
				'id' => $modal::$id,
				'has_error' => $controller->isHasError(),
			]);
			wp_die();
		}

		public static function esp_reset(){

			$nonce = filter_input(INPUT_POST, 'nonce_reset', FILTER_SANITIZE_STRING);
			$user = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING);

			$controller = new EspacePriveController;
			$controller->check_nonce($nonce, 'esp_reset');
			$controller->check_text_field($user, __("nom d'utilisateur ou email", 'wng-translate'));

			$user_datas = false;

			if(!$controller->isHasError()){
				$user_datas = $controller->get_user($user);
			}

			if ( $controller->isHasError() ){
				$modal = new Modal(
					true,
					__("Erreur lors de la réinitisaliation", 'wng-translate'),
					$controller->get_errors(),
					'reset_modal'
				);
			}

			if ( !$controller->isHasError() ){

				$modal = new Modal(
					false,
					__("Merci!", 'wng-translate'),
					__("Un lien de réinitialisation unique vous a été communiqué à votre e-mail", 'wng-translate'),
					'signin_modal'
				);

				MailHelper::send_link(
					MailHelper::RESET,
					$user_datas->user_email,
					MailHelper::subject(__("Réinitialisation de votre mot de passe", 'wng-translate')),
					MailHelper::body('password_reset'),
					MailHelper::HEADERS,
					$user_datas->ID,
				);
			}

			wp_send_json([
				'modal' => $modal::body(),
				'id' => $modal::$id,
				'has_error' => $controller->isHasError(),
			]);
			wp_die();
		}

		public static function esp_signin(){

			$nonce = filter_input(INPUT_POST, 'nonce_signin', FILTER_SANITIZE_STRING);
			$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
			$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
			$pwd = filter_input(INPUT_POST, 'pwd', FILTER_SANITIZE_STRING);

			$controller = new EspacePriveController;
			$controller->check_nonce($nonce, 'esp_signin');
			$controller->check_text_field($username,__("nom d'utilisateur", 'wng-translate'));
			$controller->check_email_field($email);
			$controller->check_text_field($pwd, __('mot de passe', 'wng-translate'));

			if(!$controller->isHasError()) { // fields ok

				// check user n'existe pas
				if($controller->check_user_not_exists($username) && $controller->check_user_not_exists($email)){

					// création de l'user
					if($user_id = $controller->register_user($username,$email, $pwd)){
						// envoi d'email de verif
						MailHelper::send_link(
							MailHelper::CONFIRM,
							$email,
							MailHelper::subject( __( "Confirmation de votre inscription à l'espace privé",'wng-translate' ) ),
							MailHelper::body('esp_confirmation'),
							MailHelper::HEADERS,
							$user_id
						);
					}
				}
			}

			// création du modal
			// TODO: Corriger wp_error avec link
			if ( $controller->isHasError() ){
				$modal = new Modal(
					true,
					__("Erreur survenue lors de votre inscription", 'wng-translate'),
					$controller->get_errors(),
					'signin_modal'
				);
			}
			if ( !$controller->isHasError() ){
				$modal = new Modal(
					false,
					__("Merci!", 'wng-translate') . $username,
					__("Veuillez vérifier vos e-mails afin de confirmer votre compte.", 'wng-translate'),
					'signin_modal'
				);
			}

			wp_send_json([
				'modal' => $modal::body(),
				'id' => $modal::$id,
				'has_error' => $controller->isHasError(),
			]);

			wp_die();
		}

		public static function esp_login(){

			$nonce = filter_input(INPUT_POST, 'nonce', FILTER_SANITIZE_STRING);
			$user = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING);
			$pwd = filter_input(INPUT_POST, 'pwd', FILTER_SANITIZE_STRING);

			$controller = new EspacePriveController;
			$controller->check_nonce($nonce, 'esp_login');

			// fields check
			$controller->check_text_field($user, __("username ou email", 'wng-translate'));
			$controller->check_text_field($pwd, __("mot de passe", 'wng-translate'));

			if(!$controller->isHasError()){ // fields ok

				// username ou email truové dans les enregistrement
				if( $controller->check_user_exists($user) ){

					// check de l'authentification
					if($controller->check_wp_error(wp_authenticate($user, $pwd))){

						// si email -> get data par email
						// sinon -> get data par login
						$user_datas = is_email($user) ? get_user_by('email', $user) : get_user_by('login', $user);

						if(!$controller->isHasError()){
							$controller->login_user($user_datas->user_login, $pwd);
						}
					}
				}
			}

			// TODO: Corriger wp_error avec link
			// création du modal
			if ( $controller->isHasError() ){
				$modal = new Modal(
					true,
					__("Erreur survenue lors de votre connection", 'wng-translate'),
					$controller->get_errors()
				);
			}
			if ( !$controller->isHasError() ){
				$modal = new Modal(
					false,
					__("Bonjour", 'wng-translate') . ' ' . $user_datas->user_login . '!',
					__("Vous êtes bien connecté à votre compte.", 'wng-translate')
				);
			}

			wp_send_json([
				'modal' => $modal::body(),
				'modal_id' => $modal::$id,
				'has_error' => $controller->isHasError(),
			]);

			wp_die();
		}
	}