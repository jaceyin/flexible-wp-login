<?php


	namespace Wng\Wplogin\Helper;

	class Modal
	{
		private static $title;
		private static $content;
		private static $error;
		public static $id;

		public function __construct($is_error, $title, $content, $id = 'info-modal'){
			self::$id = $id;
			self::$error = $is_error;
			self::$title = $title;
			self::$content= $content;
		}

		public static function body(){
			ob_start();?>
			<div class="modal micromodal-slide <?= !self::$error?: 'error' ?>" id="<?= self::$id ?>" aria-hidden="true">
				<div class="modal__overlay" tabindex="-1" data-micromodal-close>
					<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
						<header class="modal__header">
							<h2 class="modal__title" id="modal-1-title">
							<?= self::$title ?>
							</h2>
							<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
						</header>
					<main class="modal__content" id="modal-1-content">
						<?= self::$content ?>
					</main>
					<footer class="modal__footer">
						<button class="modal__btn" data-micromodal-close aria-label="Close this dialog window">Close</button>
					</footer>
					</div>
				</div>
			</div>
			<?php $content = ob_get_clean();
			return $content;
		}


	}