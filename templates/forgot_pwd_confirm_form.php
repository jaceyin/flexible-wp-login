<div id="forgot-password" class="form-content active forgot-confirm-form-content">
	<div class="" id="" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-title">
					<h4><?= __("Réinitialisation du mot de passe", 'wng-translate') ?></h4>
				</div>
				<div class="modal-body">
					<form method="post" enctype="multipart/form-data" id="forgot-form" action="">
						<div class="form_body row">
							<div class="form_field animation-label">
								<!-- login_pwd - mot de passe - pwd -->
								<label class="form_field_label" for="forgot_confirm_pwd"><?= __("Votre nouveau mot de passe", 'wng-translate') ?><span class="form_field_required">*</span></label>
								<div class="input_container input_container_text">
									<input name=forgot_confirm_pwd"" id="forgot_confirm_pwd" type="password" value="" required>
								</div>
							</div>
							<div class="form_field animation-label">
								<!-- login_pwd - mot de passe - pwd -->
								<label class="form_field_label" for="forgot_confirm_pwd_two"><?= __("Confirmer le mot de passe", 'wng-translate') ?><span class="form_field_required">*</span></label>
								<div class="input_container input_container_text">
									<input name="forgot_confirm_pwd_two" id="forgot_confirm_pwd_two" type="password" value="" required>
								</div>
							</div>
						</div>
						<div class="form_footer">
							<input type="submit" id="pwd_reset_submit" name="forgot_confirm_submit" class="button" value="<?= __("Réinitialiser", 'wng-translate') ?>">
						</div>
						<?php wp_nonce_field( 'esp_forgot_confirm', 'forgot_confirm_nonce' ); ?>
					</form>
					<p class="insc-text form-infos-text">
						<a href="<?= get_home_url() ?>" class="login-link"><?= __("Revenir à l'accueil", 'wng-translate') ?></a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>