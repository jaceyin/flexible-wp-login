<div id="tg-forgot" class="form-content forgot-form-content">
	<h4><?= __("Réinitialisation du mot de passe", 'wng-translate') ?></h4>
	<p class="form-infos-text">
		<?= __("Saisissez l'adresse e-mail ou l'username associé à votre compte et nous vous enverrons un lien pour réinitialiser votre mot de passe.", 'wng-translate') ?>
	</p>
	<form method="post" enctype="multipart/form-data" id="forgot-form" action="">
		<div class="form_body row">
			<div class="form_field animation-label">
				<!-- username - text -->
				<label class="form_field_label" for="forgot_user">
					<?= __("Nom d'utilisateur ou e-mail", 'wng-translate') ?>
					<span class="form_field_required">*</span>
				</label>
				<div class="input_container input_container_text">
					<input name="forgot_user" id="forgot_user" type="text" value="" required>
				</div>
			</div>
		</div>
		<div class="form_footer">
			<input type="submit" id="pwd_forgot_submit" name="forgot_submit" class="button" value="<?= __("Réinitialiser", 'wng-translate') ?>">
		</div>
		<?php wp_nonce_field( 'esp_reset', 'forgot_nonce' ); ?>
		<p class="insc-text form-infos-text">
			<a href="#tg-login" class="login-link toggler"><?= __("Revenir à la connexion", 'wng-translate') ?></a>
		</p>
	</form>
</div>