<div id="tg-login" class=" form-content active login-form-content">
	<h4><?= __("Connectez-vous", 'wng-translate') ?> <span class="green">.</span></h4>
	<form method="post" id="login-form" action="">
		<div class="form_body row">
			<div class="form_field animation-label">
				<!-- login_user - username ou email - text -->
				<label class="form_field_label" for="login_user"><?= __("Nom d'utilisateur ou e-mail", 'wng-translate') ?><span class="form_field_required">*</span></label>
				<div class="input_container input_container_text">
					<input name="login_user" id="login_user" type="text" value="" required>
				</div>
			</div>
			<div class="form_field animation-label">
				<!-- login_pwd - mot de passe - pwd -->
				<label class="form_field_label" for="login_pwd">
					<?= __("Mot de passe", 'wng-translate') ?><span class="form_field_required">*</span>
					<a href="#tg-forgot" class="forgot-link toggler"><?= __("Mot de passe oublié?", 'wng-translate') ?></a>
				</label>
				<div class="input_container input_container_text">
					<input name="login_pwd" id="login_pwd" type="password" value="" required>
				</div>
			</div>
		</div>
		<div class="form_footer">
			<input type="submit" id="login_submit" name="login_submit" class="button" value="<?= __("Se connecter", 'wng-translate') ?>">
		</div>
		<?php wp_nonce_field( 'esp_login', 'login_nonce' ); ?>
		<p class="insc-text form-infos-text">
			<?= __("Vous n'avez pas de compte?", 'wng-translate') ?> <a href="#tg-signin" class="insc-link toggler"><?= __("S'inscrire", 'wng-translate') ?></a>
		</p>
	</form>
</div>