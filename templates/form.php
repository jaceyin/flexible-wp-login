<?php
$template_helper = wplogin_registry()->get('template_helper');
?>

<div id="flex-login">
    <?= $template_helper::login_form() ?>
    <?= $template_helper::signin_form() ?>
    <?= $template_helper::forgot_pwd_form() ?>
</div>