<div id="tg-signin" class="form-content signin-form-content">
	<h4><?= __("Créer votre compte espace privé", 'wng-translate') ?></h4>
	<form method="post" enctype="multipart/form-data" id="signin-form" action="">
		<div class="form_body row">
			<div class="form_field animation-label">
				<!-- username - text -->
				<label class="form_field_label" for="insc_username"><?= __("Nom d'utilisateur", 'wng-translate') ?><span class="form_field_required">*</span></label>
				<div class="input_container input_container_text">
					<input name="insc_username" id="insc_username" type="text" value="" required>
				</div>
			</div>
			<div class="form_field animation-label">
				<!-- email - email -->
				<label class="form_field_label" for="insc_email"><?= __("E-mail", 'wng-translate') ?><span class="form_field_required">*</span></label>
				<div class="input_container input_container_text">
					<input name="insc_email" id="insc_email" type="email" value="" required>
				</div>
			</div>
			<div class="form_field animation-label">
				<!-- pwd - pwd -->
				<label class="form_field_label" for="insc_pwd"><?= __("Mot de passe", 'wng-translate') ?><span class="form_field_required">*</span></label>
				<div class="input_container input_container_text">
					<input name="insc_pwd" id="insc_pwd" type="password" value="" required>
				</div>
			</div>
		</div>
		<div class="form_footer">
			<input type="submit" id="signin_submit" name="insc_submit" class="button" value="<?= __("S'inscrire", 'wng-translate') ?>">
		</div>
		<?php wp_nonce_field( 'esp_signin', 'insc_nonce' ); ?>
		<p class="insc-text form-infos-text">
			<?= __("Vous avez un compte?", 'wng-translate') ?> <a href="#tg-login" class="login-link toggler"><?= __("Connexion", 'wng-translate') ?></a>
		</p>
	</form>
</div>