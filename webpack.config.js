module.exports = {
    watchOptions: {
        ignored: /node_modules/,
    },
    entry: {
        main: './assets/main.js',
        modal: './assets/modal.js'
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/dist',
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.(scss|css)$/,
                exclude: /node_modules/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            }
        ]
    },
    resolve: {
        extensions: ['*', '.js']
    }
};