import MicroModal from "micromodal";

const ajax_url = ajax.url; // localized PHP variable
const nonce = ajax.nonce; // localized PHP variable
const nonce_signin = ajax.nonce_signin; // localized PHP variable
const nonce_reset = ajax.nonce_reset; // localized PHP variable

/**
 *
 * @param {string} modal
 * @param {string} modal_id
 * @param {jQuery} $
 */
function show_modal(modal, modal_id, $)
{
    $('#page').prepend(modal);
    MicroModal.show(modal_id);

    // setTimeout(function(){
    //     MicroModal.close(modal_id);
    // }, 3000);
}

/**
 *
 * @param {string} user
 * @param {string} pwd
 * @param {jQuery} $
 * @returns
 */
const login = (user, pwd, $) => {

    const ajax = () => {

        const data = new FormData();
        data.append('action', 'esp_login');
        data.append('nonce', nonce);
        data.append('user', user);
        data.append('pwd', pwd);

        return fetch(ajax_url, {
            method: 'POST',
            credentials: 'same-origin',
            body: data
        })
        .then(response => response.json())
        .then(data => {
            show_modal(data.modal, data.modal_id, $);
        });
    };

    return ajax;
};

/**
 *
 * @param {string} username
 * @param {string} email
 * @param {string} pwd
 * @param {jQuery} $
 * @returns
 */
const signin = (username, email, pwd, $) => {

    const ajax = () => {

        const data = new FormData();
        data.append('action', 'esp_signin');
        data.append('nonce_signin', nonce_signin);
        data.append('username', username);
        data.append('email', email);
        data.append('pwd', pwd);

        return fetch(ajax_url, {
            method: 'POST',
            credentials: 'same-origin',
            body: data
        })
        .then(response => response.json())
        .then(data => {
            show_modal(data.modal, data.modal_id, $);
        });
    };

    return ajax;
};

/**
 *
 * @param {string} user
 * @param {jQuery} $
 * @returns
 */
const pwd_reset = (user, $) => {

    const ajax = () => {

        const data = new FormData();
        data.append('action', 'esp_reset');
        data.append('nonce_reset', nonce_reset);
        data.append('user', user);

        return fetch(ajax_url, {
            method: 'POST',
            credentials: 'same-origin',
            body: data
        })
        .then(response => response.json())
        .then(data => {
            show_modal(data.modal, data.modal_id, $);
        });
    };

    return ajax;
};

export { login, signin, pwd_reset };