import { login, signin, pwd_reset } from './form_ajax';

/**
 *
 * @param {jQuery} $
 * @returns
 */
const init = ($) => {

    const run = () => {

        const form_avalaible = document.getElementById('flex-login') !== null;

        if(form_avalaible) {

            form_switch();
            form_ajax();
        }
    };

    function form_switch () {

        const html_form = document.getElementById('flex-login');
        const html_tabs = $('.form-content');
        const html_toggler = $('.toggler');

        const open_tab = (target) => {
            close_elements();
            open_target(target);
        };

        const close_elements = () => {
            $(html_tabs).removeClass('active');
        };

        const open_target = (target_selector) => {
            const target = html_form.querySelector(target_selector);

            $(target).addClass('active');
        };

        $(html_toggler).on('click', function(e){
            e.preventDefault();

            const target = this.getAttribute('href');

            open_tab(target);
        });
    }

    function form_ajax () {

        const html_login_submit = $('#login_submit');
        const html_signin_submit = $('#signin_submit');
        const html_pwd_forgot_submit = $('#pwd_forgot_submit');

        $(html_login_submit).on('click', function(e){
            e.preventDefault();

            const user = $('#login_user').val();
            const pwd = $('#login_pwd').val();
            const login_req = login(user, pwd, $);

            login_req();
        });

        $(html_signin_submit).on('click', function(e){
            e.preventDefault();

            const username = $('#insc_username').val();
            const email = $('#insc_email').val();
            const pwd = $('#insc_pwd').val();
            const signin_req = signin(username, email, pwd, $);

            signin_req();
        });

        $(html_pwd_forgot_submit).on('click', function(e){
            e.preventDefault();

            const user = $('#forgot_user').val();
            const pwd_reset_req = pwd_reset(user, $);

            pwd_reset_req();
        });
    }

    return run;
};


export { init };