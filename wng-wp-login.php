<?php

/**
* Plugin Name:     Wng Wp Login
* Plugin URI:      https://wng.ch
* Description:     Ce plugin contient la logique fonctionnelle des formulaires de logins sur mesures
* Author:          Romario Sobreira
* Author URI:      https://wng.ch
* Text Domain:     wng-wp-login
* Domain Path:     /languages
* Version:         0.1.0
*
* @package         Wng_Wp_Login
*/

defined( 'ABSPATH' ) || exit;

if ( ! defined( 'WNG_WP_LOGIN' ) ) {
	define( 'WNG_WP_LOGIN', __FILE__ );
}

// composer autoloading
require_once plugin_dir_path(WNG_WP_LOGIN) . '/vendor/autoload.php';
// functions globales utiles
require_once plugin_dir_path(WNG_WP_LOGIN) . '/includes/utilities.php';
// déclaration des registres
require_once plugin_dir_path(WNG_WP_LOGIN) . '/includes/registry.php';


Wng\Wplogin\Controller\EspacePriveController::init();