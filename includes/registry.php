<?php

/**
 * Enregistrement des instanciations des Helper dans le Registry
 *
 * @author Romario Sobreira <rso@wng.ch>
 * @version 1.0
 */

/**
 * Permet d'utiliser une instance unique de Wng\Wp\Model\Registry
 *
 * @return Wng\Wplogin\Model\Registry $registry
 */
function wplogin_registry(){

    // instance static Wng\Wp\Model\Registry|NULL
    static $wplogin_registry = NULL;

    // si aucune instanciation
    if(is_null($wplogin_registry)){

        // instanciation
        $wplogin_registry = new Wng\Wplogin\Model\Registry;
    }

    return $wplogin_registry;
}

// Enregistrements des class Helper dans le Registry
// récupération via : registry()->get('mon_helper');
wplogin_registry()->add('template_helper', new Wng\Wplogin\Helper\TemplateHelper());